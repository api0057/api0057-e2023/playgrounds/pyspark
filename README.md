Pyspark Playground
==================

Install a JDK on your VM:

```bash
sudo apt install openjdk-17-jdk
```

Install the python deps and run jupyterlab:

```bash
make init
poetry run jupyter lab
```

Create an SSH tunnel to your VM:

```bash
ssh -L 8888:localhost:8888 student@xx.xxx.xxx.xx
```
